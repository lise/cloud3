export class FlightModel{
    id: number=0;
    from: string='';
    to: string='';
    departure:string='';
    arrival:string='';
}