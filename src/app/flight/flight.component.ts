import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { ApiService } from '../shared/api.service';
import { FlightModel } from './flight.model';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  formValue !: FormGroup;
  flightModelObj:FlightModel = new FlightModel();
  flightData !:any;
  showAdd!:boolean;
  showUpdate!:boolean;
  constructor(private formbuilder: FormBuilder, private api:ApiService) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      from : [''],
      to : [''],
      departure : [''],
      arrival : ['']
    })
    this.getAllFlights();
  }
  addFlightDetails(){
    this.flightModelObj.from = this.formValue.value.from;
    this.flightModelObj.to = this.formValue.value.to;
    this.flightModelObj.departure = this.formValue.value.departure;
    this.flightModelObj.arrival = this.formValue.value.arrival;

    this.api.addFlight(this.flightModelObj)
    .subscribe(res=>{
      console.log(res);
      alert("Flight Add Successfully");
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllFlights();
    },
    err=>{
      alert("Something went wrong");
    }
    )
  }
  getAllFlights(){
    this.api.getFlight()
    .subscribe(res=>{
      this.flightData =res;
    })
  }
  deleteFlight(row:any){
    this.api.deleteFlight(row.id)
    .subscribe(res=>{
      alert("Flight Deleted Successfully");
      this.getAllFlights();
    })
  }
  onEdit(row:any){
    this.showAdd=false;
    this.showUpdate=true;
    this.flightModelObj.id=row.id;
    this.formValue.controls['from'].setValue(row.from);
    this.formValue.controls['to'].setValue(row.to);
    this.formValue.controls['departure'].setValue(row.departure);
    this.formValue.controls['arrival'].setValue(row.arrival);

  }
  updateFlightDetails(){
    this.flightModelObj.from = this.formValue.value.from;
    this.flightModelObj.to = this.formValue.value.to;
    this.flightModelObj.departure = this.formValue.value.departure;
    this.flightModelObj.arrival = this.formValue.value.arrival;
    this.api.updateFlight(this.flightModelObj,this.flightModelObj.id)
    .subscribe(res=>{
      alert("Flight Updated Successfully");
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllFlights();
    })
  }

  clickAddFlight(){
    this.formValue.reset();
    this.showAdd=true;
    this.showUpdate=false;
  }

}
