import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  addFlight(data:any){
    return this.http.post<any>("https://r3jnpriav2.execute-api.us-east-2.amazonaws.com/flights",data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  getFlight(){
    return this.http.get<any>("https://r3jnpriav2.execute-api.us-east-2.amazonaws.com/flights")
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  updateFlight(data:any,id:number){
    return this.http.put<any>("https://r3jnpriav2.execute-api.us-east-2.amazonaws.com/flights/"+id,data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  deleteFlight(id:number){
    return this.http.delete<any>("https://r3jnpriav2.execute-api.us-east-2.amazonaws.com/flights/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}
